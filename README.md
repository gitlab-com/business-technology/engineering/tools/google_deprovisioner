# Google Deprovisioner

## Description

This script was created to automate the process of deprovisioning a Google Workspace User account utilizing [GitLab's CI/CD](https://docs.gitlab.com/ee/ci/). While this was created to be open source, the script does contain a few GitLab specific items. These items will be covered inside the INSTALL.md file along with recommended best practices for configuration.

The following is a list of actions the script will run on the user it is deprovisioning:

1. Reactivate the Google Account
2. Sign the user out of all devices and clear's cached entries [Google Sign Out API Reference](https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/signOut)
3. Remove the user from all Google Groups
4. Give the user's manager write access to [Google Calendar](https://support.google.com/a/users/answer/37082#Delegate_Calendar)
5. Give the user's manager [delegate access](https://support.google.com/mail/answer/138350?hl=en) to Gmail
6. Reset the account password to a 64 character randomized password
7. Remove the user's email from the [Global Address List](https://support.google.com/a/answer/166870?product_name=UnuFlow&hl=en&visit_id=638074070524669429-3112053910&rd=1&src=supportwidget0&hl=en)
8. Set the user's recover email to null
9. Set the user's recover phone number to null
10. Update the user's vacation message
11. Move the OU of the user

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Credits](#credits)
- [License](#license)

## Installation

### Prerequisites

1. GitLab account access
1. GitLab runners are configured
1. Okta API Token (Can be read only)
1. Google Workspace Service Account with the following permissions:

```bash
'https://www.googleapis.com/auth/admin.directory.user',
'https://www.googleapis.com/auth/admin.directory.group',
'https://www.googleapis.com/auth/admin.directory.user.security',
'https://www.googleapis.com/auth/gmail.settings.sharing',
'https://www.googleapis.com/auth/gmail.settings.basic',
'https://www.googleapis.com/auth/apps.licensing',
'https://www.googleapis.com/auth/drive',
'https://www.googleapis.com/auth/calendar',
'https://www.google.com/m8/feeds/contacts/'
```

### Setup

To use the script you can simply [fork](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html) the repo with [this link](https://gitlab.com/gitlab-com/business-technology/engineering/tools/google_deprovisioner/-/forks/new).

The script itself utilizes a number of ENV variables that will be required to be set in the [GitLab CI/CD](https://ops.gitlab.net/help/ci/variables/index) of the repo. The following table shows the ENV variables and their uses:

> NOTE: All CI/CD variables are of type `Variable` including the `GOOGLE_WORKSPACE_JSON_KEY`. This is due to how the [Google Auth SDK](https://gitlab.com/gitlab-it/google-auth-sdk) is configured.

|Variable|Description|Required|
|---|---|---|
| `GOOGLE_WORKSPACE_CUSTOMER_ID` | The Google Workspace Customer ID for the organization. [Find your customer ID](https://support.google.com/a/answer/10070793?product_name=UnuFlow&hl=en&visit_id=638083660519785971-1313986173&rd=1&src=supportwidget0&hl=en)  | True |
| `GOOGLE_WORKSPACE_DOMAIN` | The Google Workspace Domain for the organization. [Identify your domain host](https://support.google.com/a/answer/48323?hl=en&ref_topic=29165) | True |
| `GOOGLE_WORKSPACE_JSON_KEY` | The Google Workspace Service Account Key. This is required to have [Domain Wide Delegation](https://developers.google.com/identity/protocols/oauth2/service-account#delegatingauthority). [Creating Google Service Account and JSON Token](https://developers.google.com/identity/protocols/oauth2/service-account#creatinganaccount) | True |
| `GOOGLE_WORKSPACE_SUBJECT_EMAIL` | The subject email of the Service Account which has [Domain Wide Delegation](https://developers.google.com/identity/protocols/oauth2/service-account#delegatingauthority) | True |
| `OKTA_PROD_API_TOKEN` | Okta API token to the organizations Okta account. This can be a read only access token. [Creating Okta API Token](https://developer.okta.com/docs/guides/create-an-api-token/main/). Do not include the `SWSS ` portion of the token. This is included via the [Okta SDK](https://gitlab.com/gitlab-it/okta-sdk)| True |
| `OKTA_PROD_BASE_URL` | The URL for the Okta instance of the organization. (i.e. `https://example.okta.com`) | True |
| `SLACK_API_TOKEN` | The Slack API Token that is used for sending Slack messages. [Creating a Slack App](https://api.slack.com/authentication/basics#creating) | True |
| `SLACK_BASE_URL` | The URL for the Slack App (i.e. `https://slack.com/api`)| True |
| `SLACK_BOT_NAME` | The bot name for the Slack Application. This is what managers will see as the Slack User when messaged via the script. (i.e `Google Deprovisioner`)| True |
| `SLACK_SIGNING_SECRET` | The Signing Secret for the Slack Application | True |
| `TARGET_OU` | The Google Workspace Organization Unit (OU) to move the user deprovisioned user to | True |


## Usage

To use the script in the GitLab UI go the repository's `Pipelines`

![Navigation to Pipelines](docs/images/pipeline_navigation.png){height=200px}

In the `Pipelines` page click on the `Run Pipeline` button

![Run Pipeline Option](docs/images/pipeline_run.png)

The script will require the `GOOGLE_WORKSPACE_USER_EMAIL` env variable to be configured (Reference Picture Below)

![Run Pipeline Final](docs/images/pipeline_include_variables.png)

The script will then be run on the users email that was provided in the `GOOGLE_WORKSPACE_USER_EMAIL` env variable after clicking `Run Pipeline`.

## License

TODO

## Limitations

TODO

## Future Iteration Ideas

* Allow for alternate "manager email"
    * At times the manager of the offboarded team member would rather delegate their access to another person.
* Update the slack message to be more personalized
    * I think if we can get the user who is running the script from GitLab, we could change the message to make it sound more personal and include the name of the person who ran the script so the manager knows who to reach out to.
    * The message sent to the IT channel could also say who ran the script which would help with internal auditing if needed.
* Update the vacation message settings to be dynamic and not hard coded.

## How to Contribute

TODO

## Tests

### Testing Configuration

In order to run the test the following variables are required to be filled out. 

| Variable | Usage | Example |
| --- | --- | --- |
| `GOOGLE_WORKSPACE_TEST_CUSTOMER_ID` | The Google Workspace Customer ID to run tests on | `C123456789` |
| `GOOGLE_WORKSPACE_TEST_DOMAIN` | The Google Workspace Domain to run tests on | `example.com` |
| `GOOGLE_WORKSPACE_TEST_SUBJECT_EMAIL` | The Google Workspace email address of the service account being used to run the tests | `test_admin@example.com` |
| `GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL` | The Google Workspace email address of the manager of the testing user | `example_manager@example.com` |
| `GOOGLE_WORKSPACE_TEST_GROUP_EMAIL` | The Google Workspace Group email address that the test user will be added and removed from | `test_group@example.com` |
| `GOOGLE_WORKSPACE_TEST_USER` | The Google Workspace email address of the user that will be deprovisioned during testing | `test_user@example.com` |
| `GOOGLE_WORKSPACE_TEST_USER_ID` | The Google Workspace ID of the user that will be deprovisioned during testing | `12345678901234567890` |
| `GOOGLE_WORKSPACE_TEST_JSON_KEY` | The Service Account JSON key that will be used in the testing environment | null |
| `GOOGLE_WORKSPACE_TEST_TARGET_OU` | The ending OU that the user will be moved to during deprovisioning | `/FormerTeamMembers` |
| `GOOGLE_WORKSPACE_TEST_START_OU` | The starting OU that the user should be in before being deprovisioned | `/` |
| `GOOGLE_WORKSPACE_TEST_FILE_ID` | A file ID from a file owned by the test user being deprovisioned | `20BTEv234asvwer1_cv_t0bX23o` |
| `SLACK_TEST_CHANNEL` | The Slack channel to send the testing message to. (I have this set to my own User ID for testing) | `U1234567890` |
| `SLACK_BASE_URL` | The Base URL for the Slack Application | `https://slack.com/api` |
| `SLACK_API_TOKEN` | The Slack Application Bot Token | `xoxb-1234567890-1234567890-qwevOHIEWLJ23tJLIOU362j`|
| `OKTA_TEST_BASE_URL` | The base URL of the Okta instance that the user and manager information can be pulled from | `https://example.okta.com` |
| `OKTA_TEST_API_TOKEN` | The Okta API token that will be used to get the user and manager information | `002j-os23tqablkjIjs_Abwoijw2_a3451` |

It is recommended to set the variables inside of the GitLab CI/CD variable page. Doing so will help prevent an accidental commit of testing credentials. 

### Running Test

1. Run a pipeline on the `main` branch.
1. After the `composer` and `npm` steps are complete the `test` stage of the pipeline click the `Trigger this manual action` button.
    * This will start the test suite for the repository.
