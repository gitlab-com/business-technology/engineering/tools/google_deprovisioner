<?php

namespace App\Services\V1\Vendor\Slack;

use App\Services;

class UserService extends BaseService
{

    public function __construct($team_workspace_id = null)
    {
        // If workspace ID is null, set to default configuration value
        $team_workspace_id != null ? $team_workspace_id : config('services.slack.team_workspace_id');

        // Call BaseService methods to establish API connection
        $this->getApiConnectionVariables($team_workspace_id);
    }

    /**
     * Use Slack API to get a list of users in the Slack workspace
     *
     * @see https://api.slack.com/methods/admin.users.list
     *
     * @return object
     */
    public function list(): object
    {
        return $this->apiGetRequest('/admin.users.list');
    }

    /**
     * Use Slack API to invite a user to the Slack workspace
     *
     * @see https://api.slack.com/methods/admin.users.invite
     *
     * @param array $request_data
     *      channels                array       An array with list of channel_ids for this user to join. At least one channel is required.
     *      full_name               string      Full name of the user.
     *      email                   string      The email address of the person to invite.
     *      email_sign_in_allowed   bool        Allow invited user to sign in via email and password. Only available for Enterprise Grid teams via admin invite.
     *      custom_message          string      (Optional) An optional message to send to the user in the invite email.
     *      expires_at              datetime    (Optional) Timestamp when guest account should be disabled.
     *      multi_channel_guest     bool        (Optional) Is this user a multi-channel guest user? (Default: false)
     *      single_channel_guest    bool        (Optional) Is this user a single-channel guest user (Default: false)
     *
     * @see Due to bad naming conventions in the Slack API, we rename the fields to our nomenclature.
     *      channel_ids => channels
     *      email (no change)
     *      team_id => team_workspace_id
     *      custom_message (no change)
     *      email_password_policy_enabled => email_sign_in_allowed
     *      guest_expiration_ts => expires_at
     *      is_restricted => multi_channel_guest
     *      is_ultra_restricted => single_channel_guest
     *      real_name => full_name
     *      resend (no change)
     *
     * @return object API Response
     */
    public function invite($request_data = []): object
    {
        // Create empty array for request data to send to the API
        $api_request_data = [];

        // Loop through channels array
        foreach($channels as $channel) {
            // TODO Validate that channel exists and API credentials can grant permissions to this channel
            $validation = true;

            if($validation == true) {
                // Add channel to validated array
                $validated_channels[] = $channel;
            } else {
                // TODO Log error with granting permissions for this channel
            }
        }

        // Add validated channels comma separated string to parsed request data
        $api_request_data['channel_ids'] = implode(',', $validated_channels);

        // TODO Validate that user email exists in GLAM
        $api_request_data['real_name'] = $request_data['full_name'];
        $api_request_data['email'] = $request_data['email'];
        $api_request_data['resend'] = true;
        $api_request_data['team_id'] = $this->team_workspace_id;

        // TODO Evaluate what happens when this is true
        $api_request_data['email_password_policy_enabled'] = false;

        // Optional fields
        array_key_exists('custom_message', $request_data) ? $api_request_data['custom_message'] = $request_data['custom_message'] : '';
        array_key_exists('single_channel_guest', $request_data) ? $api_request_data['is_ultra_restricted'] = $request_data['single_channel_guest'] : '';
        array_key_exists('multi_channel_guest', $request_data) ? $api_request_data['is_restricted'] = $request_data['multi_channel_guest'] : '';
        array_key_exists('expires_at', $request_data) ? $api_request_data['guest_expiration_ts'] = $request_data['guest_expiration_ts'] : '';
        // TODO getPreciseTimestamp in Carbon. https://stackoverflow.com/questions/51572004/convert-date-to-milliseconds-in-laravel-using-carbon

        return $this->apiPostRequest('/admin.users.invite', [
            $api_request_data
        ]);
    }

    /**
     * Use Slack API to get a Slack User by the Slack User ID.
     *
     * @see https://api.slack.com/methods/users.info
     *
     * @param string $id    The ID of the Slack user (Ex. U1234567890)
     *
     * @return object       api_response object from BaseService class
     */
    public function get(string $id): object
    {
        return $this->apiGetRequest('/users.info', [
            'user' => $id,
            // 'include_local' => true
        ]);
        // TODO Test the `include_locale=true` (default false)
    }

    public function postMessage(string $uri, array $request_data = []): object|string
    {
        return $this->post($uri, $request_data);
    }


}
