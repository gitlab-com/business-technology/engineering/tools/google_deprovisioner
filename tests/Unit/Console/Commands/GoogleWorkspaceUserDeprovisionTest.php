<?php

namespace Tests\Unit\Console\Commands;

use Tests\Fakes\GoogleWorkspaceDeprovisionFake;

test('getAccountInformation() - it can get account information', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->getAccountInformation(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response->object->primaryEmail)->toBe(env('GOOGLE_WORKSPACE_TEST_USER'));
});

test('getAccountInformation() - it will return object', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->getAccountInformation(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response)->toBeObject();
    expect($success_response->object)->toBeObject();
});

test('getAccountInformation() - it will return error if account is not found', function(){
   $client = new GoogleWorkspaceDeprovisionFake();
   $client->setUp();
   $fail_response = $client->getAccountInformation('fake_email@fake.com');
   expect($fail_response->object->error->code)->toBe(404);
   expect($fail_response->object->error->message)->toBe('Resource Not Found: userKey');
});

test('moveAccountOU() - it can move user accounts OU with user email', function(){
   $client = new GoogleWorkspaceDeprovisionFake();
   $client->setUp();
   $success_response = $client->moveAccountOU(env('GOOGLE_WORKSPACE_TEST_USER'), env('GOOGLE_WORKSPACE_TEST_TARGET_OU'));
   expect($success_response->status->successful)->toBeTrue()
       ->and($success_response->object->orgUnitPath)->toBe(env('GOOGLE_WORKSPACE_TEST_TARGET_OU'));
    $client->moveAccountOU(env('GOOGLE_WORKSPACE_TEST_USER'), env('GOOGLE_WORKSPACE_TEST_START_OU'));
});

test('moveAccountOU() - it can move a user accounts OU with user id', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->moveAccountOU(env('GOOGLE_WORKSPACE_TEST_USER_ID'), env('GOOGLE_WORKSPACE_TEST_TARGET_OU'));
    expect($success_response->status->successful)->toBeTrue()
        ->and($success_response->object->orgUnitPath)->toBe(env('GOOGLE_WORKSPACE_TEST_TARGET_OU'));
    $client->moveAccountOU(env('GOOGLE_WORKSPACE_TEST_USER_ID'), env('GOOGLE_WORKSPACE_TEST_START_OU'));
});

test('moveAccountOU() - it will return an error if the OU does not exists', function(){

    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $fail_response = $client->moveAccountOU(env('GOOGLE_WORKSPACE_TEST_USER'), '/fake_ou');
    expect($fail_response->status->successful)->toBeFalse()
        ->and($fail_response->object->error->code)->toBe(400)
        ->and($fail_response->object->error->message)->toBe('Invalid Input: INVALID_OU_ID');
});

test('resetAccountPassword() - it can reset the password of a user', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->resetAccountPassword(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response->status->successful)->toBeTrue()
        ->and($success_response->status->code)->toBe(200);
});

test('removeAccountFromGlobalAddress() - it can remove the user from the Global Address List', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->removeAccountFromGlobalAddress(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response->status->successful)->toBeTrue();
    $user_information = $client->getAccountInformation(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($user_information->object->includeInGlobalAddressList)->toBeFalse();
});

test('removeAccountRecoveryEmail() - it can remove the recovery email setting', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->removeAccountRecoveryEmail(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response->object->recoveryEmail)->toBe("");
});

test('removeAccountRecoveryPhone() - it can remove the recovery phone number setting', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->removeAccountRecoveryPhone(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response->object->recoveryPhone)->toBe("");
});

test('listAllAssignedGroups() - it can list all groups a user is a member of', function() {
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->listAllAssignedGroups(env('GOOGLE_WORKSPACE_TEST_USER_ID'));
    expect($success_response->status->successful)->toBeTrue();
});

test('getMangerSlackId() - it can get the managers slack id from their Okta profile', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->getMangerInformation(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response->status->successful)->toBeTrue();
});

test('removeUserFromGroups() - it can remove user from groups', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $groups = $client->listAllAssignedGroups(env('GOOGLE_WORKSPACE_TEST_USER'));
    if(count((array) $groups->object) == 2 && property_exists($groups->object, 'etag') && property_exists($groups->object, 'kind')){
        expect($groups->object)->toBeObject();
    } else {
        foreach ($groups->object as $group) {
            $success_response = $client->removeAccountFromGroups(env('GOOGLE_WORKSPACE_TEST_USER'), $group);
            expect($success_response->status->successful)->toBeTrue();
        }
    }
});

test('createVacationMessageBody() - it can create the message body', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $vacation_body = $client->createVacationMessageBody('Manager Name', 'manager_email@example.com');

    expect($vacation_body)->toBe('Thank you for your message, I’m no longer at Gitlab. For anything business related please reach out to Manager Name at manager_email@example.com');
});

test('createVacationMessageSubject() - it can create the message subject line', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $vacation_subject = $client->createVacationMessageSubject('User Name');

    expect($vacation_subject)->toBe('User Name is no longer at GitLab');
});

test('updateVacationMessage() - it can update the vacation message', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->updateVacationMessage(env('GOOGLE_WORKSPACE_TEST_USER'), 'manager_email@example.com', 'User Name', 'Manager Name');
    expect($success_response->object->responseSubject)->toBe('User Name is no longer at GitLab')
        ->and($success_response->object->responseBodyPlainText)->toBe('Thank you for your message, I’m no longer at Gitlab. For anything business related please reach out to Manager Name at manager_email@example.com')
        ->and($success_response->status->successful)->toBeTrue();
});

test('signoutAccount() - it can sign the user out of the account', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->signoutAccount(env('GOOGLE_WORKSPACE_TEST_USER'));
    expect($success_response->status->successful)->toBeTrue()
        ->and($success_response->status->code)->toBe(204);
});

test('getDriveFiles() - it can get the users files', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->getDriveFiles();
    expect($success_response->status->successful)->toBeTrue()
        ->and($success_response->object->incompleteSearch)->toBeFalse();
});

test('grantEditorAccessToFiles() - it can grant editor access to a file', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->grantEditorAccessToFiles(env('GOOGLE_WORKSPACE_TEST_FILE_ID'), env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    expect($success_response->status->successful)->toBeTrue();
});

test('addManagerAsGmailDelegate() - it will add the manager as a delegate if not already a delegate', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();

    $delegate_exists = $client->emailDelegationExists(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));

    if($delegate_exists){
        $client->removeEmailDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    }

    $success_response = $client->addManagerAsGmailDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));

    expect($success_response->status->successful)->toBeTrue();
});

test('addManagerAsGmailDelegate() - it will return response if delegate exists', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $delegate_exists = $client->emailDelegationExists(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    if($delegate_exists->status->code == 200){
        $client->addManagerAsGmailDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    }
    $success_response = $client->addManagerAsGmailDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    expect($success_response->status->code)->toBe(200);
});

test('addGmailDelegate() - it can add a Gmail delegate', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();

    $delegate_exists = $client->emailDelegationExists(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));

    if($delegate_exists->status->code == 200){
        $client->removeEmailDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    }

    $success_response = $client->addGmailDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    expect($success_response->status->successful)->toBeTrue();
});

test('emailDelegationExists() - it will return 200 response if email delegation already exist', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $client->addManagerAsGmailDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    $delegate_exists = $client->emailDelegationExists(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));
    expect($delegate_exists->status->code)->toBe(200);
});

test('emailDelegationExists() - it will return false if email delegation does not exists', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $delegate_exists = $client->emailDelegationExists('not_a_real_email@example.com');
    expect($delegate_exists->status->code)->toBe(404);
});

test('addManagerAsCalendarDelegate() - it will set the calendar delegate', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->addManagerAsCalendarDelegate(env('GOOGLE_WORKSPACE_TEST_MANAGER_EMAIL'));

    expect($success_response->status->successful)->toBeTrue();
});

test('sendSlackMessage() - it will send a slack message to the correct channel', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $success_response = $client->sendSlackMessage(env('SLACK_TEST_CHANNEL'), 'This is a testing message');
    expect($success_response)->toBeTrue();
});

test('setManagerSlackMessage() - it will set the slack message to send to the manager', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $successful = $client->setManagerSlackMessage('Manager FullName', 'User FullName');
    expect($successful)->toBe("Hello Manager FullName,

You are receiving this notification to let you know that one of your direct reports User FullName has been deprovisioned from GitLab's Google Workspace. In keeping with our standard offboarding policy you will receive a copy of this user's Google Drive data as well as delegated access to their email and calendar account. This delegate access will remain available to you for 90 days after which the account will be closed, and all data will be archived. Please be sure to copy anything you wish to keep to your own account before this time. For more information about how to access this data please see information in <https://about.gitlab.com/handbook/business-technology/team-member-enablement/offboarding/#management|this> Handbook page

You will receive another notification 30 days before and then a final notification at 1 week before this account is closed. If you have any questions about this process, or need assistance with access the data, please feel free to reach out to the Corp IT team in the #it_help Slack channel.");
});

test('reactivateUserAccount() - it will reactivate a user account', function(){
   $client = new GoogleWorkspaceDeprovisionFake();
   $client->setUp();
   $success_response = $client->reactivateUserAccount(env('GOOGLE_WORKSPACE_TEST_USER'));
   expect($success_response->status->successful)->toBeTrue();
});

test('getTrashedEmails() - it will get all trashed emails from the user with no trashed files', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();

    $success_response = $client->getTrashedEmails();
    expect($success_response->status->successful)->toBeTrue();
});

test('getTrashedEmails() - it will list trashed emails when they exists', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();

    // Trash an email for testing purposes
    $response = $client->trashAnEmailForTesting();

    $success_response = $client->getTrashedEmails();
    expect(count($success_response->object->messages))->toBeGreaterThan(0);

    // Untrash email
    $client->untrashMessages($success_response);
});

test('untrashMessages() - it will untrash messages', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();

    // Trash an email for testing purposes
    $response = $client->trashAnEmailForTesting();

    $success_response = $client->getTrashedEmails();
    expect(count($success_response->object->messages))->toBeGreaterThan(0);

    // Untrash email
    $client->untrashMessages($success_response);

    $new_count = $client->getTrashedEmails();
    expect(property_exists($new_count->object, 'messages'))->toBeFalse();
});

test('getTrashedDriveFiles() - it will have 0 files if no files are trashed', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();

    $response = $client->getTrashedDriveFiles();
    expect(count($response->object->files))->toBe(0);
});

test('getTrashedDriveFiles() - it will list out files if they are trashed', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $trashed_response = $client->trashAFileForTesting();
    $response = $client->getTrashedDriveFiles();
    expect(count($response->object->files))->toBeGreaterThan(0);

    // Untrash the files
    $client->untrashDriveFiles($response);
});

test('untrashDriveFiles() - it will untrash a file that is in the trash', function(){
    $client = new GoogleWorkspaceDeprovisionFake();
    $client->setUp();
    $trashed_response = $client->trashAFileForTesting();
    $response = $client->getTrashedDriveFiles();
    expect(count($response->object->files))->toBeGreaterThan(0);

    // Untrash the files
    $client->untrashDriveFiles($response);
    $extra_check = $client->getTrashedDriveFiles();
    expect(count($extra_check->object->files))->toBe(0);
});
