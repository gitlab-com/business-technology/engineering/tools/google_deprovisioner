<?php

namespace App\Services\V1\Vendor\Slack;

use App\Services;

class FileService extends BaseService
{

    public function __construct($team_workspace_id = null)
    {
        // Call BaseService methods to establish API connection
        $this->getApiConnectionVariables($team_workspace_id);
    }

    /**
     * Use Slack API to get a list of users in the Slack workspace
     *
     * @see https://api.slack.com/methods/admin.users.list
     *
     * @return object
     */
    public function list(): object
    {
        return $this->apiGetRequest('/admin.users.list');
    }

    /**
     * Use Slack API to get a Slack User by the Slack User ID.
     *
     * @see https://api.slack.com/methods/users.info
     *
     * @param string $id    The ID of the Slack user (Ex. U1234567890)
     *
     * @return object       api_response object from BaseService class
     */
    public function getChannelFiles(string $id): object
    {
        return $this->apiGetRequest('/files.list', [
            'channel' => $id,
        ]);
        // TODO Test the `include_locale=true` (default false)
    }

}
