<?php


namespace App\Services\V1\Vendor\Slack;

use App\Services;
use GuzzleHttp\Promise\PromiseInterface;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class BaseService
{

    protected $base_url;
    protected $bot_token;
    protected $team_workspace_id;

    /**
     * Set the URL, Bot Token, and Workspace ID for Slack API calls.
     *
     * @param string|null $team_workspace_id The key of the array in config/services.php / gitlab
     */
    public function getApiConnectionVariables(string $team_workspace_id = null)
    {

        $this->base_url = config('services.slack.base_url');
        $this->bot_token = config('services.slack.bot_token');

        // If the team_workspace_id parameter is used then set the team_workspace_id
        // to the value of the parameter passed in. Else use the configuration file.
        $this->team_workspace_id = $team_workspace_id != null ? $team_workspace_id : config('services.slack.team_workspace_id');

        // Throw an error if the base_url is not set in the environment configuration
        if(!$this->base_url) {
            Log::channel('vendor-slack')->error('The Slack base_url is '.
                'not defined in config/services.php.',
                [
                    'log_event_type' => 'slack-api-response-error',
                    'log_class' => get_class(),
                    'error_code' => '501',
                    'error_message' => 'The Slack base_url not defined '.
                        'in config/services.php. Without this configuration, there '.
                        'is no URL to send request to.',
                    'error_reference' => 'vendor-slack-api-base-url',
                ]
            );

            abort(501, 'The Slack base_url is not defined '.
                'in config/services.php. Without this configuration, there is '.
                'no URL to send request to.');
        }

        // Throw an error if the bot_token for Slack API is not set in the
        // environment configuration.
        if(!$this->bot_token) {
            Log::channel('vendor-slack')->error('The Slack bot token is '.
                'not defined in config/services.php.',
                [
                    'log_event_type' => 'slack-api-response-error',
                    'log_class' => get_class(),
                    'error_code' => '501',
                    'error_message' => 'The Slack bot token is not defined '.
                        'in config/services.php. Without this configuration, there '.
                        'is no API base URL or API token to connect with.',
                    'error_reference' => 'vendor-slack-api-token',
                ]
            );

            abort(501, 'The Slack bot token is not defined '.
                'in config/services.php. Without this configuration, there is '.
                'no API token to connect with.');
        }

        // Throw an error if the team_workspace_id is not given or set in the
        // environment configuration.
        if(!$this->team_workspace_id) {
            Log::channel('vendor-slack')->error('The Slack workspace_id is '.
                'not defined in config/services.php.',
                [
                    'log_event_type' => 'slack-api-response-error',
                    'log_class' => get_class(),
                    'error_code' => '501',
                    'error_message' => 'The Slack workspace_id is not defined '.
                        'in config/services.php. Without this configuration, there '.
                        'the API does not know which Slack Workspace to use.',
                    'error_reference' => 'vendor-slack-api-workspace-id',
                ]
            );

            abort(501, 'The Slack bot token is not defined '.
                'in config/services.php. Without this configuration, there '.
                'the API does not know which Slack Workspace to use.');
        }
    }


    /**
     * Slack API Get Request
     * This method is called from other services to perform a GET request and return a structured object.
     *
     * Example Usage:
     * ```php
     * return $this->apiGetRequest('/files.list', [
     *       'channel' => $id,
     *  ]);
     * ```
     *
     * @param string $uri The URI with leading slash
     *
     * @param array $arguments Any additional arguments to pass into the Slack
     * GET request.
     *
     * @return object See parseApiResponse() method
     * Example Response for TODO: FILL IN HERE
     */
    public function apiGetRequest(string $uri, array $arguments = []): object
    {
        $paginated = false;

        // Use BaseService method to get API response
        $response = $this->vendorGetRequest($uri, $arguments);

        if($this->checkForCursorPagination($response) || $this->checkForPagingPagination($response)) {
            $api_response = $this->parseApiResponse($response, true);
        } else {
            $api_response = $this->parseApiResponse($response, false);
        }

        // Parse API Response and convert to returnable object with expected format
        // The checkForPagination method will return a boolean that is passed.
        return $api_response;
    }


    /**
     * Slack API Get Response
     * This method is called from the apiGetRequest method to perform the API
     * call and handle any exceptions. The response needs to be parsed using
     * the parseApiResponse method before being returned in another service class.
     *
     * @param string $uri The URI with leading slash
     *
     * @param array $arguments Any additional arguments to pass into the Slack
     * GET request.
     *
     * @return PromiseInterface|Response|void
     */
    public function vendorGetRequest(string $uri, array $arguments)
    {
        //Perform API call
        try {

            // Utilize HTTP to run a GET request against the base URL with the
            // URI supplied from the parameter appended to the end.

            $response = Http::withHeaders([
                'Authorization' => $this->bot_token
            ])->get($this->base_url.$uri.$this->generateQueryString($arguments));

            // If the response is a paginated response using cursor pagination
            if($this->checkForCursorPagination($response) == true) {

                // Resupply the url for the request to the getCursorPaginatedResults
                // helper function.
                $paginated_results = $this->getCursorPaginatedResults($this->base_url.$uri, $arguments);

                // The $paginated_results will be returned as an object of objects
                // which needs to be converted to a flat object for standardizing
                // the response returned. This needs to be a separate function
                // instead of casting to an object due to return body complexities
                // with nested array and object mixed notation.
                $response->paginated_results = $paginated_results;

                // Unset property for body and json
                unset($response->body);
                unset($response->json);

            }

            // If the response is a paginated response using paging pagination
            if($this->checkForPagingPagination($response) == true) {

                // Resupply the url for the request to the getPagingPaginatedResults
                // helper function.
                $paginated_results = $this->getPagingPaginatedResults($this->base_url.$uri, $arguments);

                // The $paginated_results will be returned as an object of objects
                // which needs to be converted to a flat object for standardizing
                // the response returned. This needs to be a separate function
                // instead of casting to an object due to return body complexities
                // with nested array and object mixed notation.
                $response->paginated_results = $paginated_results;

                // Unset property for body and json
                unset($response->body);
                unset($response->json);
            }

            // This response will still be parsed by $this->parseApiResponse()
            // which will handle the returned properties that we don't use in
            // a paginated result such as object and json from the first page
            // of results.
            return $response;

        } catch(\Illuminate\Http\Client\RequestException $e) {
            dd($e);
            // TODO: Fix error handling to give us useful logs and outputs for debugging.
        }
    }


    /**
     * Helper method for getting results requiring pagination when using the
     * cursor object for pagination.
     *
     * @see https://api.slack.com/docs/pagination
     *
     * @param string $endpoint URL endpoint for the Slack API
     *
     * @param array $arguments Additional arguments to pass into the GET request
     *
     * @return array Array of the response objects for each page combined.
     */
    public function getCursorPaginatedResults(string $endpoint, array $arguments): array
    {
        // TODO: This function can be refactored to not run the first page twice.

        $records_array = [];

        // Get a list of records
        $records = Http::withHeaders([
            'Authorization' => $this->bot_token
        ])->get($endpoint.$this->generateQueryString($arguments));

        // Get results and remove object properties (array keys) for
        // information we don't need for paginated results.
        $results = $records->object();
        unset($results->response_metadata);
        unset($results->ok);

        // Get single result from object loop.
        // This is a boring solution and can be refactored if dynamic object
        // property/keys are easier to parse.
        foreach($results as $key => $value) {
            $results_array = $value;
        }

        $records_array = array_merge($records_array, $results_array);

        // Get response metadata to get pagination cursor for next looped query
        $response_meta_data = $records->object()->response_metadata;
        $next_cursor = $response_meta_data->next_cursor;

        // Loop through paginated API requests while cursor exists (or not null)
        while($next_cursor) {

            $cursor_arguments = array_merge($arguments, ['cursor' => $next_cursor]);
            $cursor_records = Http::withHeaders([
                'Authorization' => $this->bot_token
            ])->get($endpoint.$this->generateQueryString($cursor_arguments));

            // Get cursor results and remove response_metadata and ok
            // properties (array keys) from object
            $cursor_results = $cursor_records->object();
            unset($cursor_results->response_metadata);
            unset($cursor_results->ok);

            // Get single result from object loop.
            // This is a boring solution and can be refactored if dynamic
            // object property/keys are easier to parse.
            foreach($cursor_results as $key => $value) {
                $cursor_results_array = $value;
            }
            $records_array = array_merge($records_array, $cursor_results_array);
            $next_cursor = $cursor_records->object()->response_metadata->next_cursor;
        }

        return $records_array;
    }

    /**
     * Helper method for getting results requiring pagination when using the
     * paging object for pagination.
     *
     * @see https://api.slack.com/docs/pagination
     *
     * @param string $endpoint URL endpoint for the Slack API
     *
     * @param array $arguments Additional arguments to pass into the GET request
     *
     * @return array Array of the response objects for each page combined.
     */
    public function getPagingPaginatedResults(string $endpoint, array $arguments): array
    {
        // TODO: This function can be refactored to not run the first page twice.

        $records_array = [];

        // Get a list of records
        $records = Http::withHeaders([
            'Authorization' => $this->bot_token
        ])->get($endpoint.$this->generateQueryString($arguments));

        // Get results and remove object properties (array keys) for
        // information we don't need for paginated results.
        $results = $records->object();
        unset($results->response_metadata);
        unset($results->ok);
        unset($results->paging);

        // Get single result from object loop.
        // This is a boring solution and can be refactored if dynamic object
        // property/keys are easier to parse.
        foreach($results as $key => $value) {
            $results_array = $value;
        }
        // dd($results);
        $records_array = array_merge($records_array, $results_array);

        // Get response metadata to get pagination cursor for next looped query
        $total_pages = $records->object()->paging->pages;
        $next_page = $records->object()->paging->page + 1;

        // Loop through paginated API requests while cursor exists (or not null)
        while($next_page <= $total_pages) {

            $paging_arguments = array_merge($arguments, ['page' => $next_page]);
            $paging_records = Http::withHeaders([
                'Authorization' => $this->bot_token
            ])->get($endpoint.$this->generateQueryString($paging_arguments));

            // Get cursor results and remove response_metadata and ok
            // properties (array keys) from object
            $cursor_results = $paging_records->object();
            unset($cursor_results->response_metadata);
            unset($cursor_results->ok);
            unset($cursor_results->paging);

            // Get single result from object loop.
            // This is a boring solution and can be refactored if dynamic
            // object property/keys are easier to parse.
            foreach($cursor_results as $key => $value) {
                $cursor_results_array = $value;
            }
            $records_array = array_merge($records_array, $cursor_results_array);
            $next_page = $paging_records->object()->paging->page + 1;
        }

        return $records_array;
    }

    /**
     * Check if pagination is used in the response, and is a cursor pagination
     *
     * @param Response $response API response from GitLab.
     *
     * @return bool True if the response requires multiple pages with cursor pagination| False if not
     */
    public function checkForCursorPagination(Response $response): bool
    {
        // Check if response_metadata property exists in the response object.
        if(property_exists($response->object(), 'response_metadata')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if pagination is used in the response, and is a paging pagination
     *
     * @param Response $response API response from GitLab.
     *
     * @return bool True if the response requires multiple pages with paging pagination| False if not
     */
    public function checkForPagingPagination(Response $response): bool
    {
        // Check if response_metadata property exists in the response object.
        if(property_exists($response->object(), 'paging')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Convert arguments array into query string for URL
     *
     * @param array $arguments
     *      key1 => value1
     *      key2 => value2
     *
     * @return string ?key1=value1&key2=value2
     */
    public function generateQueryString($arguments = []): string
    {
        // Create empty query string to return for for empty arrays
        $query_string = '';

        // Loop through arguments array
        foreach($arguments as $argument_key => $argument_value) {

            // If this is first key in array, prefix with `?` character
            if($argument_key === array_key_first($arguments)) {
                $query_string .= '?'.$argument_key.'='.$argument_value;
            } // For additional array keys, prefix with `&` character
            else {
                $query_string .= '&'.$argument_key.'='.$argument_value;
            }

        }

        return $query_string;
    }

    public function post(string $uri, array $request_data = []): object|string
    {
        try {
            $request = Http::withHeaders([
                'Authorization' => $this->bot_token
            ])->post($this->base_url . $uri, $request_data);

            $response = $this->parseApiResponse($request);


            return $response;
        } catch (\Illuminate\Http\Client\RequestException $exception) {
            dd($exception);
        }
    }

    /**
     * Convert API Response Headers to Object
     * This method is called from the parseApiResponse method to prettify the
     * Guzzle Headers that are an array with nested array for each value, and
     * converts the single array values into strings and converts to an object for
     * easier and consistent accessibility with the parseApiResponse format.
     *
     * @param $header_response
     * [
     *    "Date" => [
     *      "Tue, 02 Nov 2021 16:00:30 GMT",
     *    ],
     *    "Content-Type" => [
     *      "application/json",
     *    ],
     *    "Transfer-Encoding" => [
     *      "chunked",
     *    ],
     *    "Connection" => [
     *      "keep-alive",
     *    ],
     *    "Cache-Control" => [
     *      "max-age=0, private, must-revalidate",
     *    ],
     *    "Etag" => [
     *      "W/"ef80161dad0045459a87879e4d6b0769"",
     *    ],
     *    ...(truncated)
     * ]
     *
     * @return object
     *  {
     *      +"Date": "Tue, 02 Nov 2021 16:28:37 GMT",
     *      +"Content-Type": "application/json",
     *      +"Transfer-Encoding": "chunked",
     *      +"Connection": "keep-alive",
     *      +"Cache-Control": "max-age=0, private, must-revalidate",
     *      +"Etag": "W/"534830b145cda36bcd6bcd91c3ed3742"",
     *      +"Link": (truncated),
     *      +"Vary": "Origin",
     *      +"X-Content-Type-Options": "nosniff",
     *      +"X-Frame-Options": "SAMEORIGIN",
     *      +"X-Next-Page": "",
     *      +"X-Page": "1",
     *      +"X-Per-Page": "20",
     *      +"X-Prev-Page": "",
     *      +"X-Request-Id": "01FKGQPA4V7TPC70J60J72GJ30",
     *      +"X-Runtime": "0.148641",
     *      +"X-Total": "1",
     *      +"X-Total-Pages": "1",
     *      +"RateLimit-Observed": "2",
     *      +"RateLimit-Remaining": "1998",
     *      +"RateLimit-Reset": "1635870577",
     *      +"RateLimit-ResetTime": "Tue, 02 Nov 2021 16:29:37 GMT",
     *      +"RateLimit-Limit": "2000",
     *      +"GitLab-LB": "fe-14-lb-gprd",
     *      +"GitLab-SV": "localhost",
     *      +"CF-Cache-Status": "DYNAMIC",
     *      +"Expect-CT": "max-age=604800, report-uri="https://report-uri.cloudflare.com/cdn-cgi/beacon/expect-ct"",
     *      +"Strict-Transport-Security": "max-age=31536000",
     *      +"Server": "cloudflare",
     *      +"CF-RAY": "6a7ebcad3ce908db-SEA",
     *  }
     */
    public function convertHeadersToObject($header_response): object
    {
        // TODO Evaluate better way to do this. Extensive testing of collection methods failed.
        $headers = [];
        foreach($header_response as $header_key => $header_value) {
            $headers[$header_key] = implode(" ", $header_value);
        }
        return (object) $headers;
    }

    /**
     * Parse the API response and return custom formatted response for consistency
     *
     * @param object $response Response object from API results
     *
     * @param false $paginated If the response is paginated or not
     *
     * @return object Custom response returned for consistency
     *  {
     *    +"headers": {
     *      +"Date": "Fri, 12 Nov 2021 20:13:55 GMT"
     *      +"Content-Type": "application/json"
     *      +"Content-Length": "1623"
     *      +"Connection": "keep-alive"
     *    }
     *    +"json": "{"id":12345678,"name":"Dade Murphy","username":"z3r0c00l","state":"active"}"
     *    +"object": {
     *      +"id": 12345678
     *      +"name": "Dade Murphy"
     *      +"username": "z3r0c00l"
     *      +"state": "active"
     *    }
     *    +"status": {
     *      +"code": 200
     *      +"ok": true
     *      +"successful": true
     *      +"failed": false
     *      +"serverError": false
     *      +"clientError": false
     *   }
     * }
     */
    public function parseApiResponse(object $response, bool $paginated = false): object
    {
        // Use Laravel HTTP Client to return response values
        return (object) [
            'headers' => $this->convertHeadersToObject($response->headers()),
            'json' => $paginated == true ? json_encode($response->paginated_results) : json_encode($response->json()),
            'object' => $paginated == true ? (object) $response->paginated_results : $response->object(),
            'status' => (object) [
                'code' => $response->status(), // integer
                'ok' => $response->ok(), // boolean
                'successful' => $response->successful(), // boolean
                'failed' => $response->failed(), // boolean
                'serverError' => $response->serverError(), // boolean
                'clientError' => $response->clientError(), // boolean
            ],
        ];
    }
}
