<?php

namespace App\Services\Traits;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

trait ResponseLog
{
    /**
     * Helper method for logging info messages
     *
     * @param string $message
     *      The message to log
     *
     * @param array  $context
     *      Any additional context to log
     *
     * @return void
     */
    protected function logInfo(string $message, array $context = []): void
    {
        $calling_function = debug_backtrace()[2]['function'];

        $predetermined = [
            'method_name' => $calling_function,
            'class' => get_class(),
            'event_type' => 'google-deprovisioner-local-info'
        ];

        $context = array_merge($predetermined, $context);

        Log::stack($this->log_channels)->info($message, $context);
    }

    /**
     * Helper method for logging error messages
     *
     * @param string $message
     *      The message to log
     *
     * @param array  $context
     *      Any additional context to log
     *
     * @return void
     */
    protected function logError(string $message, array $context = []): void
    {
        $calling_function = debug_backtrace()[2]['function'];

        $predetermined = [
            'method_name' => $calling_function,
            'class' => get_class(),
            'event_type' => 'google-deprovisioner-local-error'
        ];
        $context = array_merge($predetermined, $context);
        Log::stack($this->log_channels)->error($message, $context);
    }
}
