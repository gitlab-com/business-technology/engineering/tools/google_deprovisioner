<?php

namespace Tests;

use Illuminate\Database\Eloquent\Factories\Factory;
use Orchestra\Testbench\TestCase as Orchestra;

class TestCase extends Orchestra
{
    protected function setUp(): void
    {
        parent::setUp();

        if (!is_dir(__DIR__.'/../vendor/orchestra/testbench-core/laravel/storage/keys')) {
            mkdir(__DIR__.'/../vendor/orchestra/testbench-core/laravel/storage/keys');
        }
        if (!is_dir(__DIR__.'/../vendor/orchestra/testbench-core/laravel/storage/keys/glamstack-google-workspace')) {
            mkdir(__DIR__.'/../vendor/orchestra/testbench-core/laravel/storage/keys/glamstack-google-workspace');
        }
        if (!is_link(__DIR__.'/../vendor/orchestra/testbench-core/laravel/storage/keys/glamstack-google-workspace/test.json')) {
            symlink(__DIR__ . '/../storage/keys/glamstack-google-workspace/test.json', __DIR__.'/../vendor/orchestra/testbench-core/laravel/storage/keys/glamstack-google-workspace/test.json');
        }

        if (!is_link(__DIR__.'/../vendor/orchestra/testbench-core/laravel/composer.lock')) {
            symlink(__DIR__ . '/../composer.lock', __DIR__.'/../vendor/orchestra/testbench-core/laravel/composer.lock');
        }

        if (!is_link(__DIR__.'/../vendor/orchestra/testbench-core/laravel/config/glamstack-okta.php')) {
            symlink(__DIR__ . '/../config/glamstack-okta.php', __DIR__.'/../vendor/orchestra/testbench-core/laravel/config/glamstack-okta.php');
        }
        if (!is_link(__DIR__.'/../vendor/orchestra/testbench-core/laravel/config/services.php')) {
            unlink(__DIR__.'/../vendor/orchestra/testbench-core/laravel/config/services.php');
            symlink(__DIR__ . '/../config/services.php', __DIR__.'/../vendor/orchestra/testbench-core/laravel/config/services.php');
        }
    }
}
