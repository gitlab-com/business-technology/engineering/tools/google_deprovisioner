<?php

namespace App\Console\Commands;

ini_set('memory_limit', -1);

use App\Services;
use App\Services\V1\Vendor\Slack\ConversationService;
use Glamstack\GoogleWorkspace\ApiClient as WorkspaceApiClient;
use Glamstack\Okta\ApiClient as OktaApiClient;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use App\Services\Traits\ResponseLog;

class GoogleWorkspaceUserDeprovision extends Command
{

    use ResponseLog;
    protected OktaApiClient $okta_api;
    protected WorkspaceApiClient $google_workspace_api;
    protected WorkspaceApiClient $user_workspace_api;
    protected array $log_channels;
    private string $user_my_drive_id;
    const FOLDER_MIME_TYPE = 'application/vnd.google-apps.folder';
    const LINK_MIME_TYPE = 'application/vnd.google-apps.shortcut';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'google-workspace-user:deprovision';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deprovision a Google Workspace User';

    /**
     * Execute the console command.
     *
     * @return int
     * @throws \Google\Exception
     */
    public function handle()
    {
        // Initialize Okta SDK
        $this->okta_api = new OktaApiClient();
        $this->log_channels = ['single'];
        $this->google_workspace_api = new WorkspaceApiClient(null,
            [
                'api_scopes' => [
                    'https://www.googleapis.com/auth/admin.directory.user',
                    'https://www.googleapis.com/auth/drive',
                    'https://www.googleapis.com/auth/apps.licensing',
                    'https://www.google.com/m8/feeds/contacts/',
                    'https://www.googleapis.com/auth/admin.directory.group',
                    'https://www.googleapis.com/auth/admin.directory.user.security',
                ],
                'json_key' => env('GOOGLE_WORKSPACE_JSON_KEY'),
                'log_channels' => ['single'],
                'customer_id' => env('GOOGLE_WORKSPACE_CUSTOMER_ID'),
                'domain' => env('GOOGLE_WORKSPACE_DOMAIN'),
                'subject_email' => env('GOOGLE_WORKSPACE_SUBJECT_EMAIL')
            ]
        );

        // Mimics API calls as the user
        $this->user_workspace_api = new WorkspaceApiClient(null,
            [
                'api_scopes' => [
                    'https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.settings.sharing',
                    'https://www.googleapis.com/auth/gmail.settings.basic',
                    'https://www.googleapis.com/auth/drive',
                    'https://www.googleapis.com/auth/calendar'
                ],
                'json_key' => env('GOOGLE_WORKSPACE_JSON_KEY'),
                'log_channels' => ['single'],
                'customer_id' => env('GOOGLE_WORKSPACE_CUSTOMER_ID'),
                'domain' => env('GOOGLE_WORKSPACE_DOMAIN'),
                'subject_email' => env('GOOGLE_WORKSPACE_USER_EMAIL')
            ]
        );

        $user_information = $this->getAccountInformation(env('GOOGLE_WORKSPACE_USER_EMAIL'));

        if(!$user_information->status->successful){
            $this->line(
                '<fg=red>Failed - To get information on ' . env('GOOGLE_WORKSPACE_USER_EMAIL') .
                ' Please check the email is correct.'
            );
            die();
        }

        // Extract the object from the response
        $user_information = $user_information->object;

        $user_email = $user_information->primaryEmail;

        $user_full_name = $user_information->name->fullName;


        // Check if the user is suspended in Google Workspace.
        if($user_information->suspended) {
            // Reactivate the users Google Workspace account
            $this->line('<fg=yellow>Reactiving '. $user_email .'\'s '.
                'Google Workspace Account');

            $account_activation = $this->reactivateUserAccount($user_email);

            if($account_activation->status->successful){
                $this->line('<fg=green>Successful - ' . $user_email . ' has been reactived.</>');
            } else {
                $this->line('<fg=red>Failed - To reactivate ' . $user_email . '.</>');
                $this->line('<fg=red>Reason - ' . $account_activation->object->message . '.</>');
            }

        } else { // Else the use is not suspended so we do nothing.
            $this->line($user_email . ' is not suspended. This script is '.
                'only able to be run on suspended accounts.</>');
            die();
        }

        sleep(3);

        if(env('GOOGLE_WORKSPACE_MANAGER_EMAIL') != null && env('GOOGLE_WORKSPACE_MANAGER_EMAIL') != ''){
            $manager_email = env('GOOGLE_WORKSPACE_MANAGER_EMAIL');
        } else {
            $manager_email = $user_information->relations[0]->value;
        }

        $this->line('<fg=blue>Starting the process of deprovisioning ' . $user_email . '</>');

        $my_drive_id_response_attempts = 0;

        do {
            $my_drive_id_response = $this->getMyDriveId();
            $my_drive_id_response_attempts = $my_drive_id_response_attempts + 1;
        } while (
            $my_drive_id_response_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $my_drive_id_response, 'Fetching the MyDrive ID for ' . $user_email
            )
        );

        $this->user_my_drive_id = $my_drive_id_response->object->id;

        // Get the manager information required
        $get_manager_information_attempts = 0;

        do {
            $manager_okta_information = $this->getManagerInformation($manager_email);
            $get_manager_information_attempts = $get_manager_information_attempts + 1;
        } while (
            $get_manager_information_attempts < 3 &&
            !$this->verifySuccessfulResponse($manager_okta_information, 'Gathering manager information from Okta.')
        );

        if($get_manager_information_attempts == 3){
            die('Failed To Get Manager Information. Either enter the manager email manually using the "GOOGLE_WORKSPACE_MANAGER_EMAIL" ENV variable, or add the manager to the user\'s Google Workspace Profile.');
        }

        $manager_full_name = $manager_okta_information->object->profile->firstName . ' ' . $manager_okta_information->object->profile->lastName;

        $slack_message = $this->setManagerSlackMessage($manager_full_name, $user_full_name);

        $get_groups_attempts = 0;

        $belongs_to_groups = true;

        $assigned_groups = $this->listAllAssignedGroups($user_email);

        do {
            $assigned_groups = $this->listAllAssignedGroups($user_email);
            $get_groups_attempts = $get_groups_attempts + 1;
        } while (
            $get_groups_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $assigned_groups, 'Gathering all groups assigned to ' . $user_email
            )
        );

        if($this->verifySuccessfulResponse($assigned_groups, 'Gathering all groups assigned to ' . $user_email)) {
            if(count((array) $assigned_groups->object) == 2 && property_exists($assigned_groups->object, 'etag') && property_exists($assigned_groups->object, 'kind')){
                $this->line('There are no groups to remove the user from');
                $belongs_to_groups = false;
            }
        }

        // Remove the user from groups
        if($belongs_to_groups){
            foreach($assigned_groups->object as $group){
                $removed_from_group_attempt = 0;
                do {
                    $removed_from_group_response = $this->removeAccountFromGroups($user_email, $group);
                    $removed_from_group_attempt = $removed_from_group_attempt + 1;
                } while (
                    $removed_from_group_attempt < 3 &&
                    !$this->verifySuccessfulResponse(
                        $removed_from_group_response, 'Removing ' . $user_email . ' from ' . $group->name
                    )
                );
            }
        }

        // Untrash Emails
        $trashed_emails = $this->getTrashedEmails();
        $this->untrashMessages($trashed_emails);

        // Untrash Drive Files
        $trashed_files = $this->getTrashedDriveFiles();
        $this->untrashDriveFiles($trashed_files);

        // Add manager as calendar delegate
        $calendar_delegation_attempts = 0;

        do {
            $calendar_delegation_response = $this->addManagerAsCalendarDelegate($manager_email);
            $calendar_delegation_attempts = $calendar_delegation_attempts + 1;
        } while (
            $calendar_delegation_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $calendar_delegation_response,
                'Adding read/write calendar delegate permissions for ' . $manager_email . ' to ' . $user_email . '\'s calendar.'
            )
        );

        // Add manager as Gmail Delegate
        $gmail_delegation_attempts = 0;

        do {
            $gmail_delegation_response = $this->addManagerAsGmailDelegate($manager_email);
            $gmail_delegation_attempts = $gmail_delegation_attempts + 1;
        } while (
            $gmail_delegation_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $gmail_delegation_response,
                'Adding read/write Gmail delegations for ' . $manager_email . ' to ' . $user_email . '\'s email.'
            )
        );

        // Reset the Account password
        $reset_password_attempts = 0;

        do {
            $reset_password_response = $this->resetAccountPassword($user_email);
            $reset_password_attempts = $reset_password_attempts + 1;
        } while (
            $reset_password_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $reset_password_response,
                'Resetting the account password for ' . $user_email . '.'
            )
        );

        // Remove from global address list
        $remove_global_address_attempts = 0;

        do {
            $remove_global_address_resposne = $this->removeAccountFromGlobalAddress($user_email);
            $remove_global_address_attempts = $remove_global_address_attempts + 1;
        } while (
            $remove_global_address_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $remove_global_address_resposne,
                'Removing ' . $user_email . ' from the global address list.'
            )
        );

        // Remove recovery email
        $remove_recovery_email_attempts = 0;

        do {
            $remove_recovery_email_response = $this->removeAccountRecoveryEmail($user_email);
            $remove_recovery_email_attempts = $remove_recovery_email_attempts + 1;
        } while (
            $remove_recovery_email_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $remove_recovery_email_response,
                'Removing the recovery email for ' . $user_email . '.'
            )
        );

        // Remove recovery phone number
        $remove_recovery_phone_attempts = 0;

        do {
            $remove_recovery_phone_response = $this->removeAccountRecoveryPhone($user_email);
            $remove_recovery_phone_attempts = $remove_recovery_phone_attempts + 1;
        } while (
            $remove_recovery_phone_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $remove_recovery_phone_response,
                'Removing the recovery email for ' . $user_email . '.'
            )
        );

        // Update the vacation message
        $update_vacation_message_attempts = 0;

        do {
            $update_vacation_message_response = $this->updateVacationMessage($user_email, $manager_email, $user_full_name, $manager_full_name);
            $update_vacation_message_attempts = $update_vacation_message_attempts + 1;
        } while (
            $update_vacation_message_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $update_vacation_message_response,
                'Updating the vacation message for ' . $user_email . '.'
            )
        );

        $get_drive_files_attempts = 0;

        do {
            $drive_files_response = $this->getDriveFiles($this->user_my_drive_id);
            $get_drive_files_attempts = $get_drive_files_attempts + 1;
        } while (
            $get_drive_files_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $drive_files_response,
                'Getting all drive files for ' . $user_email
            )
        );


        if($drive_files_response->status->successful){
            if(property_exists($drive_files_response->object, 'files') && !empty($drive_files_response->object->files)) {
                foreach($drive_files_response->object->files as $file){
                    if(is_object($file)){
                        $grant_file_access_attempts = 0;

                        do {
                            $grant_file_access_response = $this->grantEditorAccessToFiles($file->id, $manager_email);

                            $grant_file_access_attempts++;

                            if(!$grant_file_access_response->status->successful){
                                sleep(.5);

                                $this->user_workspace_api = new WorkspaceApiClient(null,
                                    [
                                        'api_scopes' => [
                                            'https://mail.google.com/',
                                            'https://www.googleapis.com/auth/gmail.settings.sharing',
                                            'https://www.googleapis.com/auth/gmail.settings.basic',
                                            'https://www.googleapis.com/auth/drive',
                                            'https://www.googleapis.com/auth/calendar'
                                        ],
                                        'json_key' => env('GOOGLE_WORKSPACE_JSON_KEY'),
                                        'log_channels' => ['single'],
                                        'customer_id' => env('GOOGLE_WORKSPACE_CUSTOMER_ID'),
                                        'domain' => env('GOOGLE_WORKSPACE_DOMAIN'),
                                        'subject_email' => env('GOOGLE_WORKSPACE_USER_EMAIL')
                                    ]
                                );
                            }
                        } while (
                            $grant_file_access_attempts < 3 &&
                            !$this->verifySuccessfulResponse(
                                $grant_file_access_response,
                                'Granting ' . $manager_email . ' read/write access to ' . $file->id
                            )
                        );
                    }
                }
            } elseif (!property_exists($drive_files_response->object, 'files') && collect($drive_files_response->object)->isNotEmpty()) {
                foreach($drive_files_response->object as $file){
                    if(is_object($file)){
                        $grant_file_access_attempts = 0;
                        do {
                            $grant_file_access_response = $this->grantEditorAccessToFiles($file->id, $manager_email);

                            $grant_file_access_attempts++;

                            if(!$grant_file_access_response->status->successful){
                                sleep(.5);
                                $this->user_workspace_api = new WorkspaceApiClient(null,
                                    [
                                        'api_scopes' => [
                                            'https://mail.google.com/',
                                            'https://www.googleapis.com/auth/gmail.settings.sharing',
                                            'https://www.googleapis.com/auth/gmail.settings.basic',
                                            'https://www.googleapis.com/auth/drive',
                                            'https://www.googleapis.com/auth/calendar'
                                        ],
                                        'json_key' => env('GOOGLE_WORKSPACE_JSON_KEY'),
                                        'log_channels' => ['single'],
                                        'customer_id' => env('GOOGLE_WORKSPACE_CUSTOMER_ID'),
                                        'domain' => env('GOOGLE_WORKSPACE_DOMAIN'),
                                        'subject_email' => env('GOOGLE_WORKSPACE_USER_EMAIL')
                                    ]
                                );
                            }
                        } while (
                            $grant_file_access_attempts < 3 &&
                            !$this->verifySuccessfulResponse(
                                $grant_file_access_response,
                                'Granting ' . $manager_email . ' read/write access to ' . $file->id
                            )
                        );
                    }
                }
            }
        }

        // Move account OU
        $move_ou_attempts = 0;

        do {
            $move_ou_response = $this->moveAccountOU($user_email, env('TARGET_OU'));
            $move_ou_attempts = $move_ou_attempts + 1;
        } while (
            $move_ou_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $move_ou_response,
                'Moving ' . $user_email . ' to the "' .  env('TARGET_OU') . '" ou.'
            )
        );

        // Sign the user out
        $signout_user_attempts = 0;

        do {
            $signout_response = $this->signoutAccount($user_email);
            $signout_user_attempts = $signout_user_attempts + 1;
        } while (
            $signout_user_attempts < 3 &&
            !$this->verifySuccessfulResponse(
                $signout_response, 'Signing ' . $user_email . ' out of GSuite.'
            )
        );

        $this->sendSlackMessage($manager_okta_information->object->profile->slack_id, $slack_message);


        $it_message = 'The initial deprovisoning script has been run on ' . $user_email . '.';

        // Send Slack Message To IT
        $this->sendSlackMessage(env('SLACK_IT_CHANNEL_ID'), $it_message);

    }

    /**
     * Get the user information from Google Workspace
     *
     * @param string $email The email of the user to deprovision.
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/get
     */
    protected function getAccountInformation(string $email): object
    {
        return $this->google_workspace_api->directory()->get('/users/' . $email);

    }

    protected function getTrashedEmails(): object
    {
        return $this->user_workspace_api->gmail()->get('/users/me/messages', [
            'q' => 'in:trash'
        ]);
    }

    protected function untrashMessages(object $trashed_messages)
    {
        if(property_exists($trashed_messages->object, 'messages')){
            foreach ($trashed_messages->object->messages as $message){
                $this->user_workspace_api->gmail()->post('/users/me/messages/' . $message->id . '/untrash');
            }
        }
    }

    protected function getTrashedDriveFiles(): object
    {
        return $this->user_workspace_api->drive()->get('/files', [
            'q' => 'trashed=true'
        ]);
    }

    protected function untrashDriveFiles(object $trashed_files)
    {
        if(property_exists($trashed_files->object,'files')){
            if(count($trashed_files->object->files) > 0){
                foreach($trashed_files->object->files as $file){
                    $this->user_workspace_api->drive()->patch('/files/' .
                    $file->id, [
                        'trashed' => false
                    ]);
                }
            }
        }
    }

    /**
     * Reactivate a user's Google Account
     *
     * @param string $user
     *     The email or id of the user to reactivate
     *
     * @return object
     *
     * @throws \Exception
     */
    protected function reactivateUserAccount(string $user): object
    {
        return $this->google_workspace_api->directory()->put('/users/' . $user,
            [
                'suspended' => false
            ]
        );
    }

    /**
     * Change the OU field of a Google Workspace User.
     *
     * @param string $user
     *      The email or id of the user to move OUs
     *
     * @param string $target_ou
     *      The OU to move the user to
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/update
     */
    protected function moveAccountOU(string $user, string $target_ou) : object
    {
        return $this->google_workspace_api->directory()->put('/users/' . $user,
            [
                'orgUnitPath' => $target_ou
            ]
        );
    }

    /**
     * Reset the users password
     *
     * @param string $user
     *      The email or id of the user to reset the password of
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/update
     */
    protected function resetAccountPassword(string $user) : object
    {
        // Create a 64 character long string for a random password.
        $newPassword = Str::random(64);

        return $this->google_workspace_api->directory()->put('/users/' . $user,
            [
                'password' => $newPassword
            ]
        );
    }

    /**
     * Remove the User from the Global Address List
     *
     * @param string $user
     *      The email or id of the user to remove from the Global Address List
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/update
     */
    protected function removeAccountFromGlobalAddress(string $user): object
    {
        return $this->google_workspace_api->directory()->put('/users/' . $user,
            [
                'includeInGlobalAddressList' => false
            ]
        );
    }

    /**
     * Remove the Google Workspace User's recovery email.
     *
     * @param string $user
     *      The email or id of the user to rest the recover email of
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/update
     */
    protected function removeAccountRecoveryEmail(string $user): object
    {
        return $this->google_workspace_api->directory()->put('/users/' . $user,
            [
                'recoveryEmail' => ""
            ]
        );
    }

    /**
     * Remove the Google Workspace User's recovery phone number.
     *
     * @param string $user
     *      The email or id of the user to reset the recover phone number of
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/update
     */
    protected function removeAccountRecoveryPhone(string $user): object
    {
        return $this->google_workspace_api->directory()->put('/users/' . $user,
            [
                'recoveryPhone' => ""
            ]
        );
    }

    /**
     * List all the groups that a user belongs to.
     *
     * @param string $user
     *      The email or id of the user to list the groups of
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/groups/list
     */
    protected function listAllAssignedGroups(string $user): object
    {
        return $this->google_workspace_api->directory()->get('/groups',
            [
                'userKey' => $user,
            ],
            false,
            true
        );
    }

    /**
     * Get the manager's information from Okta
     *
     * Utilize Okta SDK to get the managers information. Okta is used because it
     * contains the slack_id where Google does not
     *
     * @param string $manager_email
     *      The manager's email address of the user
     *
     * @return object
     */
    protected function getManagerInformation(string $manager_email): object
    {
        return $this->okta_api->get('/users/' . $manager_email);
    }

    /**
     * Set the manager Slack message
     *
     * @param string $mangaer_full_name
     *      The full name of the manager of the user being deprovisioned
     *
     * @param string $user_full_name
     *      The full name of the user being deprovisioned
     *
     * @return string
     */
    protected function setManagerSlackMessage(string $mangaer_full_name, string $user_full_name): string
    {
        return 'Hello ' . $mangaer_full_name . ',

You are receiving this notification to let you know that one of your direct reports ' . $user_full_name . ' has been deprovisioned from GitLab\'s Google Workspace. In keeping with our standard offboarding policy you will receive a copy of this user\'s Google Drive data as well as delegated access to their email and calendar account. This delegate access will remain available to you for 90 days after which the account will be closed, and all data will be archived. Please be sure to copy anything you wish to keep to your own account before this time. For more information about how to access this data please see information in <https://about.gitlab.com/handbook/business-technology/team-member-enablement/offboarding/#management|this> Handbook page

You will receive another notification 30 days before and then a final notification at 1 week before this account is closed. If you have any questions about this process, or need assistance with access the data, please feel free to reach out to the Corp IT team in the #it_help Slack channel.';
    }


    /**
     * Remove the Google Workspace User from all Google Workspace Groups.
     *
     * @param string $user
     *      The email or id of the user to remove from the groups
     *
     * @param object $group
     *      A single group from the return object from `listAllAssignedGroups`
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/members/delete
     */
    protected function removeAccountFromGroups(string $user, object $group) : object
    {
        return $this->google_workspace_api->directory()->delete('/groups/' . $group->id . '/members/' . $user);
    }

    /**
     * Update the users vacation message
     *
     * @param string $user
     *      The email or id of the user
     *
     * @param string $manager
     *      The email or id of the user's manager
     *
     * @param string $user_full_name
     *      The full name of the user
     *
     * @param string $manager_full_name
     *      The full name of the user's manager
     *
     * @return object|string
     */
    protected function updateVacationMessage(string $user, string $manager, string $user_full_name, string $manager_full_name): object|string
    {
        $this->user_workspace_api = new WorkspaceApiClient(null,
            [
                'api_scopes' => [
                    'https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.settings.sharing',
                    'https://www.googleapis.com/auth/gmail.settings.basic',
                    'https://www.googleapis.com/auth/drive',
                    'https://www.googleapis.com/auth/calendar'
                ],
                'json_key' => env('GOOGLE_WORKSPACE_JSON_KEY'),
                'log_channels' => ['single'],
                'customer_id' => env('GOOGLE_WORKSPACE_CUSTOMER_ID'),
                'domain' => env('GOOGLE_WORKSPACE_DOMAIN'),
                'subject_email' => env('GOOGLE_WORKSPACE_USER_EMAIL')
            ]
        );
        return $this->user_workspace_api->gmail()->put('/users/me/settings/vacation', [
            'enableAutoReply' => true,
            'responseSubject' => $this->createVacationMessageSubject($user_full_name),
            'responseBodyPlainText' => $this->createVacationMessageBody($manager_full_name, $manager),
            'responseBodyHtml' => null,
            'startTime' => null,
            'endTime' => null
        ]);
    }

    /**
     * Create the body of the vacation message
     *
     * @param string $manager_name
     *      The full name of the user's manager
     *
     * @param string $manager_email
     *      The email address of the user's manager
     *
     * @return string
     *
     * @see https://developers.google.com/gmail/api/reference/rest/v1/VacationSettings
     */
    protected function createVacationMessageBody(string $manager_name, string $manager_email) : string
    {
        $vacation_body = 'Thank you for your message, '.
        'I’m no longer at Gitlab. For anything business related please '.
        'reach out to ' . $manager_name . ' at ' . $manager_email;

        return $vacation_body;
    }

    /**
     * Create the subject line of the vacation message
     *
     * @param string $user_full_name
     *      The full name of the user
     *
     * @return string
     *
     * @see https://developers.google.com/gmail/api/reference/rest/v1/VacationSettings
     */
    protected function createVacationMessageSubject(string $user_full_name) : string
    {
        $vacation_subject = $user_full_name . ' is no longer at GitLab';

        return $vacation_subject;
    }

    /**
     * Sign the user out of Google and clear cache for user
     *
     * @param string $user
     *      The email or id of the user to sign out
     *
     * @return object
     *
     * @see https://developers.google.com/admin-sdk/directory/reference/rest/v1/users/signOut
     */
    protected function signoutAccount(string $user): object
    {
        return $this->google_workspace_api->directory()->post('/users/' . $user . '/signOut');
    }

    protected function getMyDriveId()
    {
        return $this->user_workspace_api->drive()->get(
            '/files/root',
            [
                'fields' => '*',
                'useDomainAdminAccess' => false
            ]
        );

    }

    /**
     * Get all MyDrive files of the user
     *
     * @return object
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/drive/api/v3/reference/permissions/list
     */
    protected function getDriveFiles(string $my_drive_id): object
    {
        return $this->user_workspace_api->drive()->get('/files', [
            'q' => 'mimeType != \'' . self::LINK_MIME_TYPE . '\'' .
                'AND mimeType != \'' . 'application/vnd.google-apps.drive-sdk.1094820495693' . '\''.
                'AND parents = \'' . $my_drive_id . '\'' .
                'AND "me" in owners',
        ]);
    }

    /**
     * Grant writer access to the Google Drive File
     *
     * NOTE: This method does not verify the email is correct or exists
     *
     * @param string $file_id
     *      The ID of the file to give the manager email editor access to
     *
     * @param string $manager_email
     *      The manager email to give RW access to
     *
     * @throws \Exception
     *
     * @see https://developers.google.com/drive/api/v3/ref-roles
     *
     * @see https://developers.google.com/drive/api/v3/reference/permissions/create
     *
     * @see https://developers.google.com/drive/api/v3/manage-sharing#capabilities
     */
    protected function grantEditorAccessToFiles(string $file_id, string $manager_email) : object
    {
        return $this->user_workspace_api->drive()->post('/files/' . $file_id . '/permissions?sendNotificationEmail=false', [
            'role' => 'writer',
            'type' => 'user',
            'emailAddress' => $manager_email
        ]);
    }

    /**
     * Set the manager as a delegate to the user's Gmail Account
     *
     * @param string $delegate_email
     *      The manager's email address to set as a delegate
     *
     * @return object
     */
    protected function addManagerAsGmailDelegate(string $delegate_email): object
    {
        // Mimics API calls as the user
        $this->user_workspace_api = new WorkspaceApiClient(null,
            [
                'api_scopes' => [
                    'https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.settings.sharing',
                    'https://www.googleapis.com/auth/gmail.settings.basic',
                    'https://www.googleapis.com/auth/drive',
                    'https://www.googleapis.com/auth/calendar'
                ],
                'json_key' => env('GOOGLE_WORKSPACE_JSON_KEY'),
                'log_channels' => ['single'],
                'customer_id' => env('GOOGLE_WORKSPACE_CUSTOMER_ID'),
                'domain' => env('GOOGLE_WORKSPACE_DOMAIN'),
                'subject_email' => env('GOOGLE_WORKSPACE_USER_EMAIL')
            ]
        );
        $delegate_exists = $this->emailDelegationExists($delegate_email);
        if($delegate_exists->status->code == 200){
            return $delegate_exists;
        } else {
            return $this->addGmailDelegate($delegate_email);
        }
    }

    /**
     * Add an email as a delegate to a Gmail account
     *
     * @param string $delegate_email The email of the user to add as a delegate.
     *
     * @return object
     *
     * @see https://developers.google.com/gmail/api/reference/rest/v1/users.settings.delegates
     */
    protected function addGmailDelegate(string $delegate_email): object
    {
        return $this->user_workspace_api->gmail()->post('/users/me/settings/delegates', [
            'delegateEmail' => $delegate_email
        ]);
    }

    /**
     * Check if email delegation already exists
     *
     * @param string $delegate_email
     *      The email address to check the user's delegations for
     *
     * @return object
     */
    protected function emailDelegationExists(string $delegate_email): object
    {
        return $this->user_workspace_api->gmail()->get('/users/me/settings/delegates/' . $delegate_email);
    }

    /**
     * Add the email as a delegate to the Google User that is being off boarded.
     *
     * @param string $delegate_email
     *      The email to add as a delegate to the Google User's calendar
     *
     * @see https://developers.google.com/calendar/api/v3/reference/acl
     */
    protected function addManagerAsCalendarDelegate(string $delegate_email)
    {
        // Mimics API calls as the user
        $this->user_workspace_api = new WorkspaceApiClient(null,
            [
                'api_scopes' => [
                    'https://mail.google.com/',
                    'https://www.googleapis.com/auth/gmail.settings.sharing',
                    'https://www.googleapis.com/auth/gmail.settings.basic',
                    'https://www.googleapis.com/auth/drive',
                    'https://www.googleapis.com/auth/calendar'
                ],
                'json_key' => env('GOOGLE_WORKSPACE_JSON_KEY'),
                'log_channels' => ['single'],
                'customer_id' => env('GOOGLE_WORKSPACE_CUSTOMER_ID'),
                'domain' => env('GOOGLE_WORKSPACE_DOMAIN'),
                'subject_email' => env('GOOGLE_WORKSPACE_USER_EMAIL')
            ]
        );
        return $this->user_workspace_api->calendar()->post('/calendars/primary/acl',
            [
                'role' => 'writer',
                'scope' => [
                    'type' => 'user',
                    'value' => $delegate_email
                ]
            ]
        );
    }

    /**
     * Send Slack Message
     *
     * @see https://api.slack.com/methods/chat.postMessage
     *
     * @param string $channel_id
     *      The channel_id to send the message to. This can be an actual
     *      channel or a Slack UserId
     *
     * @param string $message
     *      The message to send
     *
     * @return void
     */
    protected function sendSlackMessage(string $channel_id, string $message): bool
    {
        $slackConversationService = new ConversationService();
        $response = $slackConversationService->postMessage('/chat.postMessage', [
            'channel' => $channel_id,
            'text' => $message,
            'username' => env('SLACK_BOT_NAME')
        ]);

        return $response->status->successful;
    }

    protected function verifySuccessfulResponse(object $response, $message): bool
    {
         if($response->status->successful){
            $this->line('<fg=green>Success - ' . $message . '</>');
            $this->logInfo('Success - ' . $message, [
                'response_object' => $response->object
            ]);
            return true;
        } else {
            $this->line('<fg=red>Failure - ' . $message . '.</>');
            if(property_exists($response->object, 'message')){
                $this->line('<fg=red>Reason - ' . $response->object->message . '.</>');
                $this->logError('Failure - ', [
                    'response_object' => $response->object
                ]);
            }
            return false;
        }
    }
}
