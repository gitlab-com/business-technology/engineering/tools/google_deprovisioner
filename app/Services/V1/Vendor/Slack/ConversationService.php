<?php

namespace App\Services\V1\Vendor\Slack;

use App\Services;

class ConversationService extends BaseService
{

    public function __construct($team_workspace_id = null)
    {
        $team_workspace_id != null ? $team_workspace_id : config('services.slack.team_workspace_id');
        
        // Call BaseService methods to establish API connection
        $this->getApiConnectionVariables($team_workspace_id);
    }

    public function postMessage(string $uri, array $request_data = []): object|string
    {
        return $this->post($uri, $request_data);
    }
}
